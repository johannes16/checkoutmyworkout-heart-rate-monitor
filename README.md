# checkoutmyworkout-heart-rate-monitor

Javascript library to read heart rate from bluetooth devices. This library is a simple one, built on the fine work at [https://github.com/Polidea/react-native-ble-plx](https://github.com/Polidea/react-native-ble-plx).

## Getting Started

These instructions will get you up and running with the checkoutmyworkout-heart-rate-monitor library. There is a [working sample](https://gitlab.com/checkoutmyworkout/checkoutmyworkout-heart-rate-monitor-example) if you would like to start by cloning a working repository.

### Prerequisites

1. [Node.js](https://nodejs.org/en/download/)
1. [React Native](https://facebook.github.io/react-native/docs/getting-started.html)
1. [react-native-ble-plx](https://github.com/Polidea/react-native-ble-plx#configuration--installation) (be sure to have the right settings for your profile to be given Bluetooth permissions for your app)
1. An actual Bluetooth heart rate monitor.  We have tested with the [Scosche RHYTHM+](https://smile.amazon.com/gp/product/B00JQHTJS2/ref=oh_aui_search_detailpage?ie=UTF8&psc=1) and the [Polar H10](https://smile.amazon.com/gp/product/B06WV8NGTN/ref=oh_aui_search_detailpage?ie=UTF8&psc=1).

### Installing

After you have the above prerequisites, you just need to install the checkoutmyworkout-heart-rate-monitor library:

```bash
npm install checkoutmyworkout-heart-rate-monitor
```

Then use it in your code:

```js
import { HeartRateMonitor } from 'checkoutmyworkout-heart-rate-monitor';
...
HeartRateMonitor hrm = new HeartRateMonitor();

hrm.startHeartRateDevicesScan( (device, error) => {
  if (error) {
    hrm.stopHeartRateDevicesScan(); 
    console.log(`error scanning for heart rate devices: ${JSON.stringify(error)}`)
  }
  else {
    // In a real app you would probably not just take the first one,
    // you would probably add all the devices scanned to a list and let
    // your user choose.  However, for clarity, we'll just take the first
    // device and its ID here. You also wouldn't stop the monitor right 
    // away, but again, we do so for clarity in illustrating how to use 
    // the code.
    hrm.stopHeartRateDevicesScan();
    hrm.startHeartRateMonitor(device.id, (result) => {
      console.log(`result.heartRate=${result.heartRate}`);
      hrm.stopHeartRateMonitor(device.id);
    });
  }
});

```

## Acknowledgements

1. [React Native](https://facebook.github.io/react-native/docs/getting-started.html)
1. [react-native-ble-plx](https://github.com/Polidea/react-native-ble-plx#configuration--installation)
1. [contributing template](https://gist.github.com/PurpleBooth/b24679402957c63ec426) from [Billie Thompson](https://github.com/PurpleBooth)

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Brian Webb** - *Initial work* - [Brian Webb](https://gitlab.com/flacito)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
